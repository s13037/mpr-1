package usersmanager;

class Person{
    private Role role;
    private Address address;
    private String name;
    private String surname;

    public void setRole(Role r){
        this->role = r;
    }

    public Role getRole(){
        return role;
    }

    public void setAddress(Address a){
        this->address = a;
    }

    public Address getAddress(){
        return address;
    }

    public void setName(String name){
        this->name = name;
    }

    public String getName(){
        return name;
    }

    public void setSurname(String surname){
        this->surname = surname;
    }

    public String getSurname(){
        return surname;
    }
}
