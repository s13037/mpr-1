package usersmanager;

import java.util.*;

class Role{
    private rolename;
    private ArrayList<Permission> permissions = new ArrayList<Permission>();
    Role(){
    }
    public void setName(String name){
        rolename = name;
    }
    public String getName(){
        return name;
    }
    public void addPermission(Permission p){
        permissions.add(p);
    }

    public ArrayList<Permission> getPermissions(){
        return permissions;
    }
}
